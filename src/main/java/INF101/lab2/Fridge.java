package INF101.lab2;

import java.util.ArrayList;
import java.util.*;

public class Fridge implements IFridge {
    
    int maxSize = 20; 
    int amountOfItems = 0;
    ArrayList<FridgeItem> myFridge = new ArrayList<FridgeItem>();
    
    public int totalSize() {
        return maxSize;
    } 

    public int nItemsInFridge() {
        return amountOfItems;
    }
    public boolean placeIn(FridgeItem item) {
        if (amountOfItems < 20) {
            myFridge.add(item);
            amountOfItems++;
            return true;
        }
        else {
            return false;
        }
    }
    public void takeOut(FridgeItem item) {
        if (myFridge.contains(item)){
            myFridge.remove(item);
            amountOfItems--;
        }
        else {
            throw new NoSuchElementException();}

    }    
    public void emptyFridge() {
        myFridge.clear();
        amountOfItems = 0;

    }
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> myExpiredFood = new ArrayList<FridgeItem>();
        
        for (FridgeItem item : myFridge){
            if (item.hasExpired()){
                myExpiredFood.add(item);
            }
        }
        for (FridgeItem item : myExpiredFood){
            myFridge.remove(item);
            amountOfItems--;
        }
        return myExpiredFood;
    }
}
